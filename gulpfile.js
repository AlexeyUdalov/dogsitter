const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const browserSync = require('browser-sync');
const pugData = require('./data.json');
const del = require('del');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const sortMediaQueries = require('postcss-sort-media-queries');

const { reload } = browserSync;
$.sass.compiler = require('node-sass');

const config = {
  src: {
    root: 'src',
    templates: 'src/templates',
    styles: 'src/styles',
    scripts: 'src/scripts',
    assets: 'src/assets',
    images: 'src/img',
    svgSprite: 'src/sprites/svg',
    pngSprite: 'src/sprites/raster',
  },
  dest: {
    root: 'dist',
    templates: 'dist',
    styles: 'dist/css',
    scripts: 'dist/js',
    images: 'dist/img/theme',
  },
  jsConcat: [
    './node_modules/jquery/dist/jquery.min.js',
    './node_modules/jquery-validation/dist/jquery.validate.min.js',
    './node_modules/jquery-validation/dist/additional-methods.min.js',
    './node_modules/vanilla-lazyload/dist/lazyload.min.js',
    './node_modules/shave/dist/shave.min.js',
    './node_modules/list.js/dist/list.min.js',
    './node_modules/swiper/swiper-bundle.min.js',
    './node_modules/parallax-js/dist/parallax.min.js',
    './node_modules/accordion-js/dist/accordion.min.js',
    './node_modules/select2/dist/js/select2.full.min.js',
    './node_modules/air-datepicker/dist/js/datepicker.min.js',
    './node_modules/air-datepicker/dist/js/i18n/datepicker.en.js',
    './node_modules/responsive-tabs/js/jquery.responsiveTabs.min.js',
    './node_modules/sticky-js/dist/sticky.min.js',
    './node_modules/nouislider/distribute/nouislider.min.js',
    './node_modules/wnumb/wNumb.min.js',
    './node_modules/magnific-popup/dist/jquery.magnific-popup.min.js',
    './node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',
  ],
  browserSync: {
    reloadOnRestart: true,
    notify: false,
    port: 9000,
    startPath: '/',
    server: {
      baseDir: ['dist', 'src', 'src/assets'],
    },
  },
};

function errorHandler(...args) {
  // let args = Array.prototype.slice.call(arguments);
  const err = args[0];
  $.notify.onError({
    title: `Error in: ${err.plugin}`,
    message: '<%= error.message %>',
    sound: 'Submarine',
  }).apply(this, args);
  this.emit('end');
}

// compile pug
function views() {
  return gulp.src([`${config.src.templates}/**/*.pug`])
    .pipe($.plumber({ errorHandler: errorHandler }))

    // only pass unchanged *main* files and *all* the partials
    .pipe($.changed(config.dest.templates, { extension: '.html' }))

    // filter out unchanged partials, but it only works when watching
    .pipe($.if(browserSync.active, $.cached('pug')))

    // find files that depend on the files that have changed
    .pipe($.pugInheritance({ basedir: config.src.templates, extension: '.pug', skip: 'node_modules' }))

    // filter out partials (folders and files starting with "_" )
    .pipe($.filter((file) => {
      return !/\_/.test(file.path) && !/^_/.test(file.relative);
    }))

    .pipe($.pug({
      locals: pugData,
      pretty: false,
    }))
    .pipe($.beml({
      elemPrefix: '__',
      modPrefix: '--',
      modDlmtr: '-',
    }))
    .pipe($.fileInclude({ basepath: config.dest.templates }))
    .pipe(gulp.dest(config.dest.templates))
    .pipe(reload({ stream: true }));
}

function prettifyHTML() {
  return gulp.src(`${config.dest.templates}/**/*.html`)
    .pipe($.htmlPrettify({ indent_char: '  ', indent_size: 1 }))
    .pipe(gulp.dest(config.dest.templates));
}

// compile sass
function styles() {
  const plugins = [
    autoprefixer({ grid: true }),
    sortMediaQueries(),
    cssnano({
      preset: [
        'default', {
          discardComments: {
            removeAll: true,
          },
        },
      ],
    }),
  ];
  return gulp.src(`${config.src.styles}/*.{sass,scss}`)
    .pipe($.plumber({ errorHandler: errorHandler }))
    .pipe($.sourcemaps.init())
    .pipe(
      $.sass({includePaths: [config.src.styles, 'node_modules']})
    )
    .pipe(
      $.sass({
        // outputStyle: 'compressed', //nested, expanded, compact, compressed
        precision: 5,
        sourcemap: true,
        errLogToConsole: false,
      })
    )
    .pipe($.postcss(plugins))
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest(config.dest.styles))
    .pipe(reload({ stream: true }));
}

// view and check scripts
function scripts() {
  return gulp.src([`${config.src.scripts}/**/*.js`])
    .pipe($.filter((file) => {
      return !/\_/.test(file.path) && !/^_/.test(file.relative);
    }))
    .pipe($.plumber({ errorHandler: errorHandler }))
    .pipe($.sourcemaps.init())
    .pipe($.betterRollup('es'))
    .pipe($.babel({
      presets: ['@babel/env'],
    }))
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest(config.dest.scripts));
}

// concat scripts
function concatScripts() {
  return gulp.src(config.jsConcat)
    .pipe($.concat('vendors.js'))
    .pipe(gulp.dest(config.dest.scripts));
}

// sprite-gen
function pngSprite() {
  const spriteConfig = {
    imgName: 'sprite.png',
    cssName: `/${config.src.pngSprite}/_sprite.scss`,
    padding: 20,
    imgPath: '../img/theme/sprite.png',
  }

  return gulp.src(`${config.src.pngSprite}/*.png`)
    .pipe($.plumber({ errorHandler: errorHandler }))
    .pipe($.spritesmith(spriteConfig))
    .pipe(gulp.dest(config.dest.images));
}

// SVG sprite
function svgSprite() {
  const spriteConfig = {
    shape: {
      dimension: {
        maxWidth: 32,
        maxHeight: 32,
      },
      spacing: {
        padding: 0,
      },
      id: {
        generator: 'si-',
      },
    },
    mode: {
      symbol: {
        sprite: '../sprite.symbol.svg',
      },
    },
  };

  return gulp.src(`${config.src.svgSprite}/*.svg`)
    .pipe($.plumber({ errorHandler: errorHandler }))
    .pipe($.svgSprite(spriteConfig))
    .on('error', (error) => { console.log(error); })
    .pipe(gulp.dest(config.dest.images));
}

function images() {
  return gulp.src(`${config.src.images}/**/*`)
    .pipe($.imagemin())
    .pipe(gulp.dest('dist/img'));
}

// remove dist folder
function clean() {
  return del([config.dest.root]);
}

function copyAssets() {
  return gulp.src(`${config.src.assets}/**/*.*`)
    .pipe(gulp.dest(config.dest.root));
}

function copySvg() {
  return gulp.src(`${config.src.svgSprite}/*.svg`)
    .pipe(gulp.dest(`${config.dest.images}/svg-source/`));
}
function copyPng() {
  return gulp.src(`${config.src.pngSprite}/*.png`)
    .pipe(gulp.dest(`${config.dest.images}/png-source/`));
}

const copy = gulp.series(
  copyAssets,
  copySvg,
  copyPng,
);

function watch(cb) {
  browserSync.init(config.browserSync);

  // watch for changes
  gulp.watch([
    `${config.dest.scripts}/**/*.js`,
    'src/img/**/*',
  ]).on('change', reload);

  gulp.watch([`${config.src.scripts}/**/*.js`], gulp.series('scripts'));
  gulp.watch('src/vendors/**/*.js', gulp.series('concatScripts'));
  gulp.watch(`${config.src.styles}/**/*.scss`, gulp.series('styles'));
  gulp.watch('src/**/*.pug', gulp.series('views'));
  gulp.watch(`${config.src.pngSprite}/**/*.png`, gulp.series('pngSprite'));
  gulp.watch(`${config.src.svgSprite}/**/*.svg`, gulp.series('svgSprite', 'views'));

  cb();
}

// main task
const serve = gulp.series(
  clean,
  views,
  styles,
  scripts,
  concatScripts,
  pngSprite,
  svgSprite,
  watch,
);

const build = gulp.series(
  clean,
  views,
  prettifyHTML,
  styles,
  scripts,
  concatScripts,
  images,
  svgSprite,
  pngSprite,
  copy,
);

exports.svgSprite = svgSprite;
exports.pngSprite = pngSprite;
exports.scripts = scripts;
exports.concatScripts = concatScripts;
exports.styles = styles;
exports.copy = copy;
exports.views = views;
exports.prettifyHTML = prettifyHTML;
exports.serve = serve;
exports.build = build;
exports.clean = clean;
exports.watch = watch;
exports.images = images;
exports.default = serve;
