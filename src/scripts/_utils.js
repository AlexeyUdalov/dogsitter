const utils = {
  cache: {
    fileInputs: new Map(),
  },
  // сет брейкпоинтов для js
  // должны совпадать с теми что в body:after
  mediaBreakpoint: {
    sm: 576,
    md: 768,
    lg: 1200,
    xl: 1366,
    xxl: 1920,
  },
  isMobile: {
    Android() {
      return navigator.userAgent.match(/Android/i);
    },
    BlackBerry() {
      return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS() {
      return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera() {
      return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows() {
      return navigator.userAgent.match(/IEMobile/i);
    },
    any() {
      return (this.Android() || this.BlackBerry() || this.iOS() || this.Opera() || this.Windows());
    },
  },
  svgIcons() {
    const container = document.querySelector('[data-svg-path]');
    const path = container.getAttribute('data-svg-path');
    const xhr = new XMLHttpRequest();
    xhr.onload = () => {
      container.innerHTML = xhr.responseText;
    };
    xhr.open('get', path, true);
    xhr.send();
  },
  detectIE() {
    /**
     * detect IE
     * returns version of IE or false, if browser is not Internet Explorer
     */

     (function detectIE() {
       var ua = window.navigator.userAgent;

       var msie = ua.indexOf('MSIE ');
       if (msie > 0) {
        // IE 10 or older => return version number
        var ieV = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        document.querySelector('body').className += ' IE';
      }

      var trident = ua.indexOf('Trident/');
      if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        var ieV = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        document.querySelector('body').className += ' IE';
      }

      var edge = ua.indexOf('Edge/');
      if (edge > 0) {
        // IE 12 (aka Edge) => return version number
        var ieV = parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        document.querySelector('body').className += ' IE';
      }

      // other browser
      return false;
    })();
  },
  truncateText(elements) {
    const cutText = () => {
      for (let i = 0; i < elements.length; i++) {
        const text = elements[i];
        const elemMaxHeight = parseInt(getComputedStyle(text).maxHeight, 10);
        const elemHeight = text.offsetHeight;
        const maxHeight = elemMaxHeight ? elemMaxHeight : elemHeight;

        shave(text, maxHeight);
      }
    };

    this.cache.cutTextListener = this.throttle(cutText, 100);

    cutText();

    window.addEventListener('resize', this.cache.cutTextListener);
  },
  throttle(callback, limit) {
    let wait = false;

    return () => {
      if (!wait) {
        callback.call();
        wait = true;
        setTimeout(() => {
          wait = false;
        }, limit);
      }
    };
  },
  getScreenSize() {
    let screenSize = window.getComputedStyle(document.querySelector('body'), ':after').getPropertyValue('content');
    screenSize = parseInt(screenSize.match(/\d+/), 10);
    return screenSize;
  },
  polyfills() {
    /**
     * polyfill for elem.closest
     */
    (function(ELEMENT) {
      ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector || ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;
      ELEMENT.closest = ELEMENT.closest || function closest(selector) {
        if (!this) return null;
        if (this.matches(selector)) return this;
        if (!this.parentElement) {
          return null;
        } else {
          return this.parentElement.closest(selector);
        } 
      };
    }(Element.prototype));

    /**
     * polyfill for elem.hasClass
     */
    Element.prototype.hasClass = function(className) {
      return this.className && new RegExp("(^|\\s)" + className + "(\\s|$)").test(this.className);
    };

    Number.isNaN = Number.isNaN || function(value) {
      return typeof value === 'number' && isNaN(value);
    }

    // Шаги алгоритма ECMA-262, 6-е издание, 22.1.2.1
    // Ссылка: https://people.mozilla.org/~jorendorff/es6-draft.html#sec-array.from
    if (!Array.from) {
      Array.from = (function() {
        var toStr = Object.prototype.toString;
        var isCallable = function(fn) {
          return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
        };
        var toInteger = function (value) {
          var number = Number(value);
          if (isNaN(number)) { return 0; }
          if (number === 0 || !isFinite(number)) { return number; }
          return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
        };
        var maxSafeInteger = Math.pow(2, 53) - 1;
        var toLength = function (value) {
          var len = toInteger(value);
          return Math.min(Math.max(len, 0), maxSafeInteger);
        };

        // Свойство length метода from равно 1.
        return function from(arrayLike/*, mapFn, thisArg */) {
          // 1. Положим C равным значению this.
          var C = this;

          // 2. Положим items равным ToObject(arrayLike).
          var items = Object(arrayLike);

          // 3. ReturnIfAbrupt(items).
          if (arrayLike == null) {
            throw new TypeError('Array.from requires an array-like object - not null or undefined');
          }

          // 4. Если mapfn равен undefined, положим mapping равным false.
          var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
          var T;
          if (typeof mapFn !== 'undefined') {
            // 5. иначе
            // 5. a. Если вызов IsCallable(mapfn) равен false, выкидываем исключение TypeError.
            if (!isCallable(mapFn)) {
              throw new TypeError('Array.from: when provided, the second argument must be a function');
            }

            // 5. b. Если thisArg присутствует, положим T равным thisArg; иначе положим T равным undefined.
            if (arguments.length > 2) {
              T = arguments[2];
            }
          }

          // 10. Положим lenValue равным Get(items, "length").
          // 11. Положим len равным ToLength(lenValue).
          var len = toLength(items.length);

          // 13. Если IsConstructor(C) равен true, то
          // 13. a. Положим A равным результату вызова внутреннего метода [[Construct]]
          //     объекта C со списком аргументов, содержащим единственный элемент len.
          // 14. a. Иначе, положим A равным ArrayCreate(len).
          var A = isCallable(C) ? Object(new C(len)) : new Array(len);

          // 16. Положим k равным 0.
          var k = 0;
          // 17. Пока k < len, будем повторять... (шаги с a по h)
          var kValue;
          while (k < len) {
            kValue = items[k];
            if (mapFn) {
              A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
            } else {
              A[k] = kValue;
            }
            k += 1;
          }
          // 18. Положим putStatus равным Put(A, "length", len, true).
          A.length = len;
          // 20. Вернём A.
          return A;
        };
      }());
    }
  },
};

export default utils;