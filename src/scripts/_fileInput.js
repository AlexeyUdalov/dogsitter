class FileInput {
  static createImage(file) {
    const img = document.createElement('img');

    img.src = URL.createObjectURL(file);
    img.alt = file.name;
    img.setAttribute('data-object-fit', 'cover');

    return img;
  }

  static createVideo(file) {
    const video = document.createElement('video');

    video.src = URL.createObjectURL(file);
    video.setAttribute('data-object-fit', 'cover');

    return video;
  }

  static createListItem(file, index) {
    const div = document.createElement('div');

    div.className = 'file-input__item responsive-img';
    div.setAttribute('data-index', index);

    if (/^image\//.test(file.type)) {
      div.append(FileInput.createImage(file));
    } else if (/^video\//.test(file.type)) {
      div.append(FileInput.createVideo(file));
    }

    return div;
  }

  constructor(el) {
    this._files = [];
    this.el = el;
    this.max = el.dataset.max;
    this.input = el.querySelector('.js-file-input-control');
    // this.imageList = el.querySelector('.js-file-input-list');
    this.dropdown = el.querySelector('.js-file-input-dropdown');
    this.dropdownValue = this.dropdown.querySelector('.js-file-input-dropdown-value');
    this.dropdownList = this.dropdown.querySelector('.js-file-input-dropdown-list');

    this.input.addEventListener('change', this.addFiles.bind(this));
  }

  getFiles() {
    return this._files;
  }

  addFiles() {
    const newFiles = this.input.files;

    for (const file of newFiles) {
      const listItem = FileInput.createListItem(file, this._files.length);
      this._files.push(file);

      if (this._files.length <= this.max) {
        this.dropdown.before(listItem);
      } else {
        this.updateDropdown(listItem);
      }
    }
  }

  updateDropdown(el) {
    this.dropdownValue.innerText = `+${this._files.length - this.max}`;
    this.dropdownList.append(el);

    if (this.dropdown.classList.contains('hidden')) {
      this.dropdown.classList.remove('hidden');
    }
  }
}

export default FileInput;
