const states = {
  TYPE_ACTIVE: 'active',
  ITEM_ANIMATED: ['animated-show', 'animated-hide'],
};
const elements = {
  FILTERS_TYPE: '[data-type-filter]',
  CURRENT_TYPE: `[data-type-filter].${states.TYPE_ACTIVE}`,
  ITEMS: '[data-type]',
};

class MyList {
  constructor(parent, id, optionsList = []) {
    this._id = id;
    this._optionsList = optionsList;
    this._parent = parent;
    this.list = [];

    this._initList();
  }

  get id() {
    return this._id;
  }

  _initListPlugin() {
    const options = {
      valueNames: [
        {data: ['type'],}
      ],
    };

    if (this._optionsList.length) {
      options.valueNames = options.valueNames.concat(...this._optionsList);
    }

    this.list = new List(this._id, options);
  }

  itemsToggleAnimated(boolean) {
    const list = this._parent.querySelector('.js-list');

    if (boolean) {
      list.classList.remove(states.ITEM_ANIMATED[1]);
      list.classList.add(states.ITEM_ANIMATED[0]);
    } else {
      list.classList.remove(states.ITEM_ANIMATED[0]);
      list.classList.add(states.ITEM_ANIMATED[1]);
    }
  }

  filterList() {
    const currentType = this._parent.querySelector(elements.CURRENT_TYPE).dataset.typeFilter;

    if (currentType !== 'all') {
      this.list.filter((item) => {
        return currentType === item.values().type;
      });
    } else {
      this.list.filter();
    }

    this.itemsToggleAnimated(true);
  }

  navItemRemoveActive() {
    const navItemsArr = Array.from(this._parent.querySelectorAll(elements.FILTERS_TYPE));

    navItemsArr.forEach((item) => {
      item.classList.remove(states.TYPE_ACTIVE);
    });
  }

  _initList() {
    if (!document.getElementById(this._id)) {
      console.error(`Не найден список с id=${this._id}`);
      return;
    }
    this._initListPlugin();
    this.filterList();
  }
}

export default MyList;