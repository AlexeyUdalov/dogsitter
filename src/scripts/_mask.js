class Mask {
  constructor(selector, regExpString = '+7 (___) ___-__-__') {
    this.elements = document.querySelectorAll(selector);
    this.regExpString = regExpString;
  }

  init() {
    this._listeners(this.elements);
  }

  _listeners(selector) {
    for (let i = 0; i < selector.length; i++) {
      const input = selector[i];
      input.addEventListener('input', this._mask.bind(this), false);
      input.addEventListener('focus', this._mask.bind(this), false);
      input.addEventListener('blur', this._mask.bind(this), false);
    }
  }

  _setCursorPosition(pos, elem) {
    elem.focus();
    if (elem.setSelectionRange) {
      elem.setSelectionRange(pos, pos);
    } else if (elem.createTextRange) {
      const range = elem.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  }

  _mask(event) {
    let matrix = this.regExpString,
      i = 0,
      def = matrix.replace(/\D/g, ''),
      val = event.target.value.replace(/\D/g, '');
    if (def.length >= val.length) {
      val = def;
    }
    event.target.value = matrix.replace(/./g, function(a) {
      return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? '' : a
    });
    if (event.type == 'blur') {
      if (event.target.value.length == 2) {
        event.target.value = '';
      }
    } else {
      this._setCursorPosition(event.target.value.length, event.target);
    }
  }
}

export default Mask;
