class CheckboxSelect {
  constructor(el) {
    this.el = el;
    this.toggle = this.el.querySelector('.js-select-checkbox-toggle');
    this.valueEl = this.el.querySelector('.js-select-checkbox-value');
    this.checkboxes = Array.from(this.el.querySelectorAll('input[type="checkbox"]'));
  }

  init() {
    const that = this;

    this.valueEl.textContent = this.checkboxes.filter((item) => item.checked).length;

    this.toggle.addEventListener('click', (e) => {
      this.el.classList.toggle('active');
    });

    this.checkboxes.forEach((item) => {
      item.addEventListener('change', this.changeCurrentValue.bind(this));
    });

    document.addEventListener('click', (e) => {
      if (!$(e.target).closest(that.el).length && that.el.classList.contains('active')) {
        that.el.classList.remove('active');
      }
    });
  }

  changeCurrentValue() {
    this.valueEl.textContent = this.checkboxes.filter((item) => item.checked).length;
  }

  documentClickHandler() {

  }
}

export default CheckboxSelect;
