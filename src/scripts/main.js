import Mask from './_mask';
import utils from './_utils';
import MyList from './_myList';
import CheckboxSelect from './_checkboxSelect';
import FileInput from './_fileInput';

const APP = {
  initBefore() {
    utils.polyfills();
    utils.svgIcons();
    document.documentElement.className = document.documentElement.className.replace('no-js', 'js');
  },

  init() {
    utils.detectIE();
    APP.lazyload();

    const myMask = new Mask('.js-tel');
    myMask.init();

    APP.buttons();
    // APP.closeOnFocusLost();

    this.initParallax();
    this.initFirstSlider();
    this.initReviewsSlider();
    this.initArticlesSlider();
    this.initProfileSlider();
    this.initAccordion();
    this.initCalendar();
    this.initTabs();
    this.initPriceList();
    this.addFormValidate();
    this.initSelect();
    this.initStarsSelect();
    this.initRatingSelect();
    this.initCheckboxSelect();
    this.initPerformersMap();
    this.initPerformersFilter();
    this.initRangeSLider();
    this.initModals();
    this.initGallery();

    this.initFileInput();
    this.initAvatarInput();
    this.initScrollBlock();
    this.initAddPetBlock();
    this.initPetCardEvents();
    this.initBirthdayCalendar();
    this.initDeleteBlock();
  },

  initOnLoad() {
    utils.truncateText(document.querySelectorAll('.js-dot'));
  },

  buttons() {
    Array.prototype.forEach.call(
      document.querySelectorAll('.js-mobile-menu-button'), (item) => {
        item.addEventListener('click', () => {
          document.body.classList.toggle('nav-showed');
        });
      },
    );
    Array.from(document.querySelectorAll('.js-link-anchor')).forEach((item) => {
      item.addEventListener('click', (e) => {
        e.preventDefault();
        const position = $($(e.currentTarget).attr('href')).offset().top;

        $('html, body').animate({
          scrollTop: `${position}px`,
        }, {
          duration: 500,
          easing: 'swing',
        });
      });
    });
  },

  // closeOnFocusLost() {
  //   document.addEventListener('click', (e) => {
  //     const trg = e.target;
  //     if (!trg.closest('.header')) {
  //       document.body.classList.remove('nav-showed');
  //     }
  //   });
  // },

  lazyload() {
    function update() {
      APP.myLazyLoad.update();
      APP.objectFitFallback(document.querySelectorAll('[data-object-fit]'));
    }

    function regularInit() {
      APP.myLazyLoad = new LazyLoad({
        elements_selector: '.lazyload',
        callback_error(el) {
          el.parentElement.classList.add('lazyload-error');
        },
      });
      APP.objectFitFallback(document.querySelectorAll('[data-object-fit]'));
    }

    if (typeof APP.myLazyLoad === 'undefined') {
      regularInit();
    } else {
      update();
    }
  },

  objectFitFallback(selector) {
    // if (true) {
    if ('objectFit' in document.documentElement.style === false) {
      for (let i = 0; i < selector.length; i++) {
        const that = selector[i];
        const imgUrl = that.getAttribute('src') ? that.getAttribute('src') : that.getAttribute('data-src');
        const dataFit = that.getAttribute('data-object-fit');
        let fitStyle;
        if (dataFit === 'cover') {
          fitStyle = 'cover';
        } else {
          fitStyle = 'contain';
        }
        const parent = that.parentElement;
        if (imgUrl) {
          parent.style.backgroundImage = `url(${imgUrl})`;
          parent.classList.add('fit-img');
          parent.classList.add(`fit-img--${fitStyle}`);
        }
      }
    }
  },

  initParallax() {
    const scenes = Array.from(document.querySelectorAll('.js-scene'));

    scenes.forEach((item) => {
      const parallaxInstance = new Parallax(item);
    });
  },

  initFirstSlider() {
    const sliderBlock = document.querySelector('.js-first-slider');

    if (sliderBlock) {
      const firstSlider = new Swiper(sliderBlock, {
        speed: 800,
        effect: 'fade',
        fadeEffect: {
          crossFade: true,
        },
        watchOverflow: true,
        navigation: {
          prevEl: '.js-first-prev-button',
          nextEl: '.js-first-next-button',
        },
        pagination: {
          el: '.js-first-pagination',
        },
      });
    }
  },

  initReviewsSlider() {
    const sliderBlock = document.querySelector('.js-reviews-slider');
    const thumbSLiderBlock = document.querySelector('.js-thumb-reviews-slider');

    if (sliderBlock && thumbSLiderBlock) {
      const thumbSLider = new Swiper(thumbSLiderBlock, {
        watchOverflow: true,
        speed: 800,
        effect: 'fade',
        allowTouchMove: false,
        fadeEffect: {
          crossFade: true,
        },
      });

      const slider = new Swiper(sliderBlock, {
        watchOverflow: true,
        speed: 800,
        spaceBetween: 30,
        thumbs: {
          swiper: thumbSLider,
        },
        navigation: {
          prevEl: '.js-reviews-prev-button',
          nextEl: '.js-reviews-next-button',
        },
        pagination: {
          el: '.js-reviews-pagination',
        },
      });
    }
  },

  initArticlesSlider() {
    const sliderBlock = document.querySelector('.js-articles-slider');

    if (sliderBlock) {
      const articlesSlider = new Swiper(sliderBlock, {
        speed: 800,
        watchOverflow: true,
        spaceBetween: 43,
        slidesPerView: 1,
        navigation: {
          prevEl: '.js-articles-prev-button',
          nextEl: '.js-articles-next-button',
        },
        pagination: {
          el: '.js-articles-pagination',
        },
        breakpoints: {
          1200: {
            slidesPerView: 3,
          },
        },
      });
    }
  },

  initProfileSlider() {
    const sliderBlock = document.querySelector('.js-profile-slider');

    if (sliderBlock) {
      const profileSlider = new Swiper(sliderBlock, {
        speed: 800,
        slidesPerView: 1,
        spaceBetween: 10,
        navigation: {
          prevEl: '.js-profile-prev',
          nextEl: '.js-profile-next',
        },
        breakpoints: {
          768: {
            slidesPerView: 'auto',
          },
        },
      });
    }
  },

  initAccordion() {
    if (document.querySelector('.js-accordion')) {
      const accordion = new Accordion('.js-accordion', {
        closeOthers: false,
      });
    }
  },

  initCalendar() {
    const calendarBlocks = Array.from(document.querySelectorAll('.js-calendar'));

    if (calendarBlocks.length) {
      calendarBlocks.forEach((calendar) => {
        const datePicker = calendar.querySelector('.js-datepicker');
        const startDateinput = calendar.querySelector('.js-start-date');
        const endDateinput = calendar.querySelector('.js-end-date');

        $(datePicker).datepicker({
          range: true,
          dateFormat: 'd M yyyy',
          minDate: new Date(),
          onSelect(formattedDate) {
            const [startDate, endDate] = formattedDate.split(',');

            startDateinput.value = startDate;

            if (endDate) {
              endDateinput.value = endDate;
            }
          },
        });
      });
    }
  },

  initBirthdayCalendar(el = document) {
    const calendars = Array.from(el.querySelectorAll('.js-birthday-calendar'));

    if (calendars.length) {
      calendars.forEach((item) => {
        const { myDate } = item.dataset;

        $(item).datepicker({
          autoClose: true,
        });

        if (myDate) {
          $(item).datepicker().data('datepicker').selectDate(new Date(myDate));
        }
      });
    }
  },

  initTabs() {
    const tabBlocks = Array.from(document.querySelectorAll('.js-tabs'));

    if (tabBlocks.length) {
      tabBlocks.forEach((block) => {
        $(block).responsiveTabs({
          duration: 500,
          setHash: block.hasAttribute('data-hash'),
          activate(evt, tab) {
            const tabThumb = block.querySelector('.js-tabs-thumb');
            if (tabThumb) {
              setTimeout(() => {
                const activeTab = tab.tab[0];
                const navigationCoords = activeTab.parentElement.getBoundingClientRect();
                const activeTabCoords = activeTab.getBoundingClientRect();

                tabThumb.style.width = `${activeTab.offsetWidth}px`;
                tabThumb.style.left = `${activeTabCoords.left - navigationCoords.left}px`;
              }, 100);
            }
          },
        });
      });
    }
  },

  initPriceList() {
    const listBlock = document.querySelector('.js-price-list');

    if (listBlock) {
      const listToggles = Array.from(listBlock.querySelectorAll('.js-list-toggle'));
      const priceList = new MyList(listBlock, listBlock.dataset.id);

      listToggles.forEach((item) => {
        item.addEventListener('click', (evt) => {
          evt.preventDefault();
          if (!evt.currentTarget.classList.contains('active')) {
            priceList.navItemRemoveActive();
            priceList.itemsToggleAnimated(false);
            evt.currentTarget.classList.add('active');
            priceList.filterList();
          }
        });
      });
    }
  },

  addFormValidate() {
    const forms = Array.from(document.querySelectorAll('form.js-validate'));

    if (forms.length) {
      forms.forEach((el) => {
        if (!el.classList.contains('validation')) {
          this.formValidate(el);
        }
      });
    }
  },

  formValidate(el) {
    const $form = $(el);
    el.classList.add('validation');

    $.validator.methods.email = function(value, element) {
      return this.optional(element) || /[a-z]+@[a-z]+\.[a-z]+/.test(value);
    };

    $.validator.addMethod('compareValue', function(value, element) {
      const compareValue = this.findByName(element.dataset.compareName).val();
      return value === compareValue;
    });

    $form.validate({
      wrapper: 'p',
      errorElement: 'span',
      rules: {
        phone: {
          mobileRU: true,
        },
        email: {
          email: true,
        },
        passwordRepeat: {
          compareValue: true,
        },
        'repeat-new-pass': {
          compareValue: true,
        },
      },
      errorPlacement(error, element) {
        if (element.attr('type') === 'checkbox') {
          return element.next('label').append(error);
        }
        return false;
      },
      highlight(element) {
        $(element).addClass('error').parent().addClass('error');
      },
      unhighlight(element) {
        $(element).removeClass('error').parent().removeClass('error');
      },
      submitHandler(form) {
        const formData = new FormData(form);
        const fileInput = form.querySelector('.js-file-input');

        if (fileInput) {
          const cacheFileInput = utils.cache.fileInputs.get(fileInput);
          formData.set('files', cacheFileInput.getFiles());
        }

        $.ajax({
          url: form.action,
          data: formData,
          processData: false,
          contentType: false,
          type: 'POST',
          success: () => {
            form.reset();
            $.magnificPopup.close();
          },
        });
      },
    });
  },

  selectOptions: {
    minimumResultsForSearch: Infinity,
    width: 'resolve',
  },

  initSelect(el) {
    const doc = el || document;
    const selects = Array.from(doc.querySelectorAll('.js-select'));

    if (selects.length) {
      const options = { ...APP.selectOptions };
      selects.forEach((item) => {
        if (item.hasAttribute('data-placeholder')) {
          APP.selectOptions.placeholder = item.dataset.placeholder;
        }

        $(item).select2({
          ...options,
          dropdownParent: $(item).parent(),
        });

        $(item).on('select2:select', (e) => {
          const select = e.currentTarget;
          select.classList.remove('error');
          select.parentElement.classList.remove('error');
        });
      });
    }
  },

  initStarsSelect() {
    const selectStars = Array.from(document.querySelectorAll('.js-select-stars'));

    const formatStarState = (state) => {
      if (!state.id) {
        return state.text;
      }

      const MAX_STARS = 5;
      const { element } = state;
      const { stars, quantity } = element.dataset;
      let starString = '';

      for (let i = 1; i <= MAX_STARS; i++) {
        if (i > stars) {
          starString += '<span class="star"><i class="fas fa-star"></i></span>';
        } else {
          starString += '<span class="star star--yellow"><i class="fas fa-star"></i></span>';
        }
      }

      return $(`<span>${starString} (${quantity})</span>`);
    };

    if (selectStars.length) {
      const options = { ...APP.selectOptions };

      selectStars.forEach((item) => {
        if (item.hasAttribute('data-placeholder')) {
          options.placeholder = item.dataset.placeholder;
        }

        $(item).select2({
          ...options,
          dropdownParent: $(item).parent(),
          templateResult: formatStarState,
          templateSelection: formatStarState,
        });

        $(item).on('select2:select', (e) => {
          const select = e.currentTarget;
          select.classList.remove('error');
          select.parentElement.classList.remove('error');
        });
      });
    }
  },

  initRatingSelect() {
    const selectRating = Array.from(document.querySelectorAll('.js-select-rating'));

    const formatStarStateResult = (state) => {
      if (!state.id) {
        return 'state.text';
      }

      const { element } = state;
      const { stars } = element.dataset;
      let starString = '';

      for (let i = 1; i <= stars; i++) {
        starString += '<span class="star star--yellow"><i class="fas fa-star"></i></span>';
      }

      return $(`<span>${starString}</span>`);
    };

    const formatStarStateSelection = (state) => {
      if (!state.id) {
        return state.text;
      }

      const { element } = state;
      const { stars } = element.dataset;
      let starString = '';

      for (let i = 1; i <= stars; i++) {
        starString += '<span class="star star--yellow"><i class="fas fa-star"></i></span>';
      }

      return $(`<span>Рейтинг от ${starString}</span>`);
    };

    if (selectRating.length) {
      const options = { ...APP.selectOptions };
      selectRating.forEach((item) => {
        if (item.hasAttribute('data-placeholder')) {
          options.placeholder = item.dataset.placeholder;
        }

        $(item).select2({
          ...options,
          dropdownParent: $(item).parent(),
          templateResult: formatStarStateResult,
          templateSelection: formatStarStateSelection,
        });

        $(item).on('select2:select', (e) => {
          const select = e.currentTarget;
          select.classList.remove('error');
          select.parentElement.classList.remove('error');
        });
      });
    }
  },

  performersDataMap: {
    sticky: null,
  },

  initPerformersMap() {
    const stickyMap = document.querySelector('.js-sticky-map');

    if (stickyMap) {
      if (utils.getScreenSize() >= utils.mediaBreakpoint.lg) {
        this.performersDataMap.sticky = new Sticky('.js-sticky-map');
      }

      window.addEventListener('resize', () => {
        if (utils.getScreenSize() >= utils.mediaBreakpoint.lg && !APP.performersDataMap.sticky) {
          APP.performersDataMap.sticky = new Sticky('.js-sticky-map');
        } else if (utils.getScreenSize() < utils.mediaBreakpoint.lg && APP.performersDataMap.sticky) {
          APP.performersDataMap.sticky.destroy();
          APP.performersDataMap.sticky = null;
        }
      });
    }
  },

  initPerformersFilter() {
    const filter = document.querySelector('.js-performers-filter');

    if (filter) {
      const toggleButton = filter.querySelector('.js-performers-filter-toggle');

      toggleButton.addEventListener('click', (e) => {
        e.preventDefault();
        filter.classList.toggle('active');
      });
    }
  },

  initCheckboxSelect() {
    const selects = Array.from(document.querySelectorAll('.js-select-checkbox'));

    if (selects.length) {
      selects.forEach((item) => {
        const select = new CheckboxSelect(item);

        select.init();
      });
    }
  },

  initRangeSLider() {
    const sliders = Array.from(document.querySelectorAll('.js-range'));

    if (sliders.length) {
      sliders.forEach((item) => {
        const inputs = Array.from(item.parentElement.querySelectorAll('.js-range-value'));
        const {
          min,
          max,
          step,
          startMin,
          startMax,
        } = item.dataset;

        noUiSlider.create(item, {
          start: [parseFloat(startMin), parseFloat(startMax)],
          connect: true,
          step: parseFloat(step),
          range: {
            min: parseFloat(min),
            max: parseFloat(max),
          },
          tooltips: [true, true],
          format: wNumb({
            decimals: step > 0 ? 0 : 1,
          }),
        });

        if (inputs.length) {
          const inputMin = inputs[0];
          const inputMax = inputs[1];
          item.noUiSlider.on('update', (values, handle) => {
            const value = values[handle];
            if (handle) {
              inputMax.value = value;
            } else {
              inputMin.value = value;
            }
          });
        }
      });
    }
  },

  initModals() {
    const callbacks = {
      ajaxContentAdded() {
        const $modalContent = $(this.content);

        if ($modalContent.find('.js-validate').length) {
          $modalContent.find('.js-validate').each((id, el) => {
            APP.formValidate(el);
          });
        }

        if ($modalContent.find('.js-select').length) {
          APP.initSelect(this.content[0]);
        }

        if ($modalContent.find('.js-tel')) {
          const myMask = new Mask('.js-tel');
          myMask.init();
        }

        if ($modalContent.find('.js-file-input')) {
          const { fileInputs } = utils.cache;
          $modalContent.find('.js-file-input').each((id, el) => {
            fileInputs.set(el, new FileInput(el));
          });
        }
      },
      open() {
        document.body.classList.remove('nav-showed');
      },
      close() {
        const $modalContent = $(this.content);

        if ($modalContent.find('.js-file-input')) {
          const { fileInputs } = utils.cache;
          $modalContent.find('.js-file-input').each((id, el) => {
            fileInputs.delete(el);
          });
        }
      },
    };

    const options = {
      removalDelay: 500,
      mainClass: 'mfp-fade',
      fixedContentPos: true,
      fixedBgPos: true,
      showCloseBtn: false,
    };

    $('.js-ajax-modal').magnificPopup({
      type: 'ajax',
      callbacks,
      ...options,
    });

    $('.js-inline-modal').magnificPopup({
      type: 'inline',
      ...options,
      callbacks: {
        open() {
          const $modalContent = $(this.content);

          if ($modalContent.find('.js-tabs')) {
            $modalContent.find('.js-tabs').each((id, el) => {
              $(el).responsiveTabs('activate', 0);
            });
          }
        },
      },
    });
  },

  initGallery() {
    const galleries = Array.from(document.querySelectorAll('.js-gallery'));

    if (galleries.length) {
      galleries.forEach((gallery) => {
        $(gallery).magnificPopup({
          type: 'image',
          delegate: 'a',
          removalDelay: 500,
          mainClass: 'mfp-fade',
          fixedContentPos: true,
          fixedBgPos: true,
          gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1],
          },
          callbacks: {
            elementParse(item) {
              const { type } = item.el[0].dataset;
              item.type = type;
            },
          },
        });
      });
    }
  },

  initFileInput(el) {
    const { fileInputs } = utils.cache;
    const doc = el || document;
    const elements = Array.from(doc.querySelectorAll('.js-file-input'));

    if (elements.length) {
      elements.forEach((item) => {
        fileInputs.set(item, new FileInput(item));
      });
    }
  },

  initAvatarInput() {
    const avatarInputs = Array.from(document.querySelectorAll('.js-avatar-input'));

    function createImage() {
      const img = document.createElement('img');
      img.setAttribute('data-object-fit', 'cover');

      return img;
    }

    if (avatarInputs.length) {
      avatarInputs.forEach((inputBlock) => {
        inputBlock.addEventListener('change', (e) => {
          const file = e.target.files[0];
          let img = e.currentTarget.querySelector('img');

          if (img) {
            img.src = URL.createObjectURL(file);
          } else {
            img = createImage();
            img.src = URL.createObjectURL(file);
            e.currentTarget.querySelector('.js-avatar-image').append(img);
          }
        });
      });
    }
  },

  initScrollBlock() {
    const scrollBlocks = Array.from(document.querySelectorAll('.js-scroll-block'));

    if (scrollBlocks.length) {
      scrollBlocks.forEach((item) => {
        const { axis = 'y' } = item.dataset;

        $(item).mCustomScrollbar({
          axis,
        });
      });
    }
  },

  initAddPetBlock() {
    const addPetBlock = document.querySelector('.js-add-pet');

    function toggleAddPetBlock() {
      const { width, left } = addPetBlock.getBoundingClientRect();
      const { clientWidth } = document.documentElement;
      const translateX = clientWidth - left - width;

      addPetBlock.style.transform = `translate3d(${translateX}px, 0, 0)`;
      addPetBlock.classList.toggle('active');
    }

    function closeAddPetBlock() {
      addPetBlock.style.transform = 'translate3d(0, 0, 0)';
      addPetBlock.classList.remove('active');
    }

    if (addPetBlock) {
      const toggleButton = document.querySelector('.js-add-pet-toggle');

      toggleButton.addEventListener('click', (e) => {
        e.preventDefault();
        toggleAddPetBlock();
      });

      document.addEventListener('click', (e) => {
        if (!e.target.closest('.js-add-pet.active') && addPetBlock.classList.contains('active')) {
          closeAddPetBlock();
        }
      });

      window.addEventListener('resize', () => {
        if (addPetBlock.classList.contains('active')) {
          closeAddPetBlock();
        }
      });
    }
  },

  initDeleteBlock(el = document) {
    const CONTENT_PADDING_BOTTOM = 40;
    const blocks = Array.from(el.querySelectorAll('.js-delete'));

    function toggleActiveDeleteHandler(e) {
      e.preventDefault();
      const deleteBlock = e.currentTarget.closest('.js-delete');
      const { top, height } = deleteBlock.getBoundingClientRect();
      const deleteDropdown = deleteBlock.querySelector('.js-delete-dropdown');
      const isTopDropdown = document.documentElement.clientHeight - height - top - CONTENT_PADDING_BOTTOM < deleteDropdown.offsetHeight;

      blocks
        .filter((item) => item.classList.contains('active') && item !== deleteBlock)
        .forEach((item) => item.classList.remove('active'));

      if (!deleteBlock.classList.contains('active')) {
        if (isTopDropdown) {
          deleteDropdown.classList.add('b-delete__dropdown--top');
        } else {
          deleteDropdown.classList.remove('b-delete__dropdown--top');
        }
      }

      deleteBlock.classList.toggle('active');
    }

    function closeDeleteHandler(e) {
      e.preventDefault();
      const deleteBlock = e.currentTarget.closest('.js-delete');

      deleteBlock.classList.remove('active');
    }

    function deleteHandler(e) {
      e.preventDefault();
      const deleteButton = e.currentTarget;
      const deleteBlock = deleteButton.closest('.js-delete');
      const { classElement, url } = deleteButton.dataset;
      const deleteElement = deleteBlock.closest(`.${classElement}`);

      deleteElement.classList.add('deleted');
      deleteBlock.classList.remove('active');

      $.get(url)
        .done(() => {
          const toggleButton = deleteBlock.querySelector('.js-delete-toggle');
          const noButton = deleteBlock.querySelector('.js-delete-no');
          const yesButton = deleteBlock.querySelector('.js-delete-yes');

          toggleButton.removeEventListener('click', toggleActiveDeleteHandler);
          noButton.removeEventListener('click', closeDeleteHandler);
          yesButton.removeEventListener('click', deleteHandler);

          deleteElement.remove();
        })
        .fail(() => {
          deleteElement.classList.remove('deleted');
        });
    }

    if (blocks.length) {
      blocks.forEach((block) => {
        const toggle = block.querySelector('.js-delete-toggle');
        const noButton = block.querySelector('.js-delete-no');
        const yesButton = block.querySelector('.js-delete-yes');

        toggle.addEventListener('click', toggleActiveDeleteHandler);
        noButton.addEventListener('click', closeDeleteHandler);
        yesButton.addEventListener('click', deleteHandler);
      });

      document.addEventListener('click', (e) => {
        if (!e.target.closest('.js-delete.active')) {
          blocks
            .filter((item) => item.classList.contains('active'))
            .forEach((item) => item.classList.remove('active'));
        }
      });
    }
  },

  initPetCardEvents() {
    const cards = Array.from(document.querySelectorAll('.js-pet-card'));

    if (cards.length) {
      cards.forEach((item) => {
        const toggleButton = item.querySelector('.js-pet-card-toggle');
        const hideBlock = item.querySelector('.js-pet-card-hide');
        const { activeContent, inactiveContent } = toggleButton.dataset;

        toggleButton.addEventListener('click', (e) => {
          e.preventDefault();
          e.stopPropagation();
          const activeCards = Array.from(document.querySelectorAll('.js-pet-card.active')).filter((card) => card !== item);

          if (activeCards.length) {
            activeCards.forEach((card) => {
              const hideBlockOther = card.querySelector('.js-pet-card-hide');
              card.classList.remove('active');
              $(hideBlockOther).slideUp();
            });
          }

          item.classList.toggle('active');

          if (item.classList.contains('active')) {
            $(hideBlock).slideDown();
            this.textContent = activeContent;
          } else {
            $(hideBlock).slideUp();
            this.textContent = inactiveContent;
          }
        });
      });

      document.addEventListener('click', (e) => {
        if (!e.target.closest('.js-pet-card.active')) {
          const activeEl = document.querySelector('.js-pet-card.active');
          if (activeEl) {
            const hideBlock = activeEl.querySelector('.js-pet-card-hide');
            const toggleButton = activeEl.querySelector('.js-pet-card-toggle');
            const { inactiveContent } = toggleButton.dataset;
            activeEl.classList.remove('active');
            $(hideBlock).slideUp();
            toggleButton.textContent = inactiveContent;
          }
        }
      });
    }
  },
};

APP.initBefore();

document.addEventListener('DOMContentLoaded', () => {
  APP.init();
});

window.onload = () => {
  APP.initOnLoad();
};
